package pl.springcoder.bankeventsourced.application;

import org.springframework.stereotype.Service;
import pl.springcoder.bankeventsourced.domain.AccountData;
import pl.springcoder.bankeventsourced.domain.AccountDoesNotExistsException;
import pl.springcoder.bankeventsourced.domain.AccountNo;
import pl.springcoder.bankeventsourced.domain.AccountRepository;

@Service
public class AccountQueryService {
    
    private AccountRepository accountRepository;
    
    public AccountQueryService(AccountRepository anAccountRepository) {
        accountRepository = anAccountRepository;
    }
    
    public AccountData details(String anAccountNo) throws AccountDoesNotExistsException {
        return accountRepository.withAccountNo(new AccountNo(anAccountNo)).state();
    }
}
