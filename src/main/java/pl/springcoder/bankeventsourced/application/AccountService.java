package pl.springcoder.bankeventsourced.application;

import org.springframework.stereotype.Service;
import pl.springcoder.bankeventsourced.application.command.DepositCommand;
import pl.springcoder.bankeventsourced.application.command.NewAccountCommand;
import pl.springcoder.bankeventsourced.application.command.TransferCommand;
import pl.springcoder.bankeventsourced.application.command.WithdrawCommand;
import pl.springcoder.bankeventsourced.domain.*;

@Service
public class AccountService {
    
    private AccountRepository accountRepository;
    
    public AccountService(AccountRepository anAccountRepository) {
        accountRepository = anAccountRepository;
    }
    
    public AccountData create(NewAccountCommand newAccountCommand) {
        AccountCurrency accountCurrency = newAccountCommand.getAccountCurrency();
        Account account = new Account(accountCurrency);
        
        accountRepository.save(account);
        
        return account.state();
    }
    
    public void withdraw(WithdrawCommand withdrawCommand)
            throws InsufficientBalanceException, AccountDoesNotExistsException {
        AccountNo accountNo = withdrawCommand.getAccountNo();
        double amount = withdrawCommand.getAmount();
        
        Account account = account(accountNo);
        account.withdraw(amount);
    }
    
    public void deposit(DepositCommand depositCommand) throws AccountDoesNotExistsException {
        AccountNo accountNo = depositCommand.getAccountNo();
        double amount = depositCommand.getAmount();
        
        Account account = account(accountNo);
        account.deposit(amount);
    }
    
    public void transfer(TransferCommand transferCommand) throws InsufficientBalanceException,
                                                                         IncompatibilityAccountCurrencyException,
                                                                         AccountDoesNotExistsException {
        Account srcAccount = account(new AccountNo(transferCommand.getSourceAccountNo()));
        Account destAccount = account(new AccountNo(transferCommand.getDestinationAccountNo()));
        
        srcAccount.transfer(destAccount, transferCommand.getAmount());
    }
    
    private Account account(AccountNo accountNo) throws AccountDoesNotExistsException {
        return accountRepository.withAccountNo(accountNo);
    }
}
