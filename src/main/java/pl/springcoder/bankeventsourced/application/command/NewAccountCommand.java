package pl.springcoder.bankeventsourced.application.command;

import pl.springcoder.bankeventsourced.domain.AccountCurrency;

public final class NewAccountCommand {
    private final AccountCurrency accountCurrency;
    
    public NewAccountCommand(AccountCurrency anAccountCurrency) {
        accountCurrency = anAccountCurrency;
    }
    
    public AccountCurrency getAccountCurrency() {
        return accountCurrency;
    }
}
