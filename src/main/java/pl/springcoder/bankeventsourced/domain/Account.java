package pl.springcoder.bankeventsourced.domain;

import pl.springcoder.bankeventsourced.domain.event.AccountCreated;
import pl.springcoder.bankeventsourced.domain.event.AccountDeposited;
import pl.springcoder.bankeventsourced.domain.event.AccountTransferred;
import pl.springcoder.bankeventsourced.domain.event.AccountWithdrawn;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import java.util.Date;

public final class Account extends EventSourcedRootEntity {
    
    private transient AccountNo accountNo;
    private transient volatile MonetaryAmount balance;
    private transient AccountCurrency currency;
    
    public Account(AccountCurrency anAccountTypeCurrency) {
        this(new AccountNo(), anAccountTypeCurrency, 0);
    }
    
    private Account(AccountNo anAccountNo, AccountCurrency anAccountTypeCurrency, double aBalance) {
        this.apply(new AccountCreated(anAccountNo, anAccountTypeCurrency, aBalance, new Date()));
    }
    
    public synchronized void withdraw(double amount) throws InsufficientBalanceException {
        throwExceptionIfInsufficientBalance(amount);
        this.apply(new AccountWithdrawn(accountNo(), amount));
    }
    
    public synchronized void deposit(double amount) {
        this.apply(new AccountDeposited(accountNo(), amount));
    }
    
    public synchronized void transfer(Account destination, double amount) throws InsufficientBalanceException, IncompatibilityAccountCurrencyException {
        throwExceptionIfIncompatibilityAccountCurrency(destination);
        throwExceptionIfInsufficientBalance(amount);
        
        AccountTransferred aDomainEvent = new AccountTransferred(accountNo(), destination.accountNo, amount);
        
        this.apply(aDomainEvent);
        destination.apply(aDomainEvent);
    }
    
    boolean isBalanceEqualTo(double anAmount) {
        return balance().isEqualTo(money(anAmount));
    }
    
    public AccountData state() {
        return new AccountData(accountNoValue(), currency(), balanceString(), mutatingEvents());
    }
    
    public AccountNo accountNo() {
        return accountNo;
    }
    
    protected void when(AccountCreated accountCreated) {
        setAccountNo(accountCreated.accountNo());
        setCurrency(accountCreated.accountCurrency());
        setBalance(accountCreated.balance());
    }
    
    protected void when(AccountWithdrawn accountWithdrawn) {
        assertArgumentEquals(accountNo(), accountWithdrawn.accountNo(), "must be equal");
        setBalance(balance.subtract(money(accountWithdrawn.amount())));
    }
    
    protected void when(AccountDeposited accountDeposited) {
        assertArgumentEquals(accountNo(), accountDeposited.accountNo(), "must be equal");
        setBalance(balance.add(money(accountDeposited.amount())));
    }
    
    protected void when(AccountTransferred accountTransferred) {
        if (accountNo().equals(accountTransferred.source())) {
            assertArgumentEquals(accountNo(), accountTransferred.source(), "must be equal");
            setBalance(balance.subtract(money(accountTransferred.amount())));
        } else if (accountNo().equals(accountTransferred.dest())) {
            assertArgumentEquals(accountNo(), accountTransferred.dest(), "must be equal");
            setBalance(balance.add(money(accountTransferred.amount())));
        }
    }
    
    private void throwExceptionIfInsufficientBalance(double amount) throws InsufficientBalanceException {
        if (balance().isLessThan(money(amount))) {
            throw new InsufficientBalanceException();
        }
    }
    
    private void throwExceptionIfIncompatibilityAccountCurrency(Account destinationAccount)
            throws IncompatibilityAccountCurrencyException {
        if (currency() != destinationAccount.currency()) {
            throw new IncompatibilityAccountCurrencyException();
        }
    }
    
    private void setCurrency(AccountCurrency accountTypeCurrency) {
        this.currency = accountTypeCurrency;
    }
    
    private void setAccountNo(AccountNo accountNo) {
        this.accountNo = accountNo;
    }
    
    private void setBalance(double aBalance) {
        this.assertArgumentRange(aBalance, 0, Double.MAX_VALUE, "Balance can't be less than zero!");
        this.setBalance(money(aBalance));
    }
    
    private void setBalance(MonetaryAmount aBalance) {
        if (aBalance.isNegative()) {
            throw new IllegalArgumentException("Balance can't be less than zero!");
        }
        this.balance = aBalance;
    }
    
    private MonetaryAmount money(double anAmount) {
        return Monetary.getDefaultAmountFactory().setNumber(anAmount).setCurrency(currency().toString()).create();
    }
    
    private AccountCurrency currency() {
        return currency;
    }
    
    private String accountNoValue() {
        return accountNo.accountNo();
    }
    
    private MonetaryAmount balance() {
        return balance;
    }
    
    private String balanceString() {
        return balance.toString();
    }
}
