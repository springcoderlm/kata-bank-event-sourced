package pl.springcoder.bankeventsourced.domain;

public enum AccountCurrency {
    EUR,
    PLN
}
