package pl.springcoder.bankeventsourced.domain;

import java.util.List;

public final class AccountData {
    private final String accountNo;
    private final AccountCurrency currency;
    private final String balance;
    private final List<DomainEvent> events;
    
    public AccountData(String accountNo, AccountCurrency currency, String balance, List<DomainEvent> events) {
        this.accountNo = accountNo;
        this.currency = currency;
        this.balance = balance;
        this.events = events;
    }
    
    public String getAccountNo() {
        return accountNo;
    }
    
    public AccountCurrency getCurrency() {
        return currency;
    }
    
    public String getBalance() {
        return balance;
    }
    
    public List<DomainEvent> getEvents() {
        return events;
    }
}
