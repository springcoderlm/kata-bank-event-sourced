package pl.springcoder.bankeventsourced.domain.event;

import pl.springcoder.bankeventsourced.domain.AccountCurrency;
import pl.springcoder.bankeventsourced.domain.AccountNo;
import pl.springcoder.bankeventsourced.domain.DomainEvent;

import java.util.Date;

public final class AccountCreated implements DomainEvent {
    private final AccountNo accountNo;
    private final AccountCurrency accountCurrency;
    private final double balance;
    private final Date occurredOn;
    
    public AccountCreated(AccountNo accountNo, AccountCurrency accountCurrency, double balance, Date occuredOn) {
        this.accountNo = accountNo;
        this.accountCurrency = accountCurrency;
        this.balance = balance;
        this.occurredOn = occuredOn;
    }
    
    public AccountNo accountNo() {
        return accountNo;
    }
    
    public AccountCurrency accountCurrency() {
        return accountCurrency;
    }
    
    public double balance() {
        return balance;
    }
    
    @Override
    public int eventVersion() {
        return 0;
    }
    
    @Override
    public Date occurredOn() {
        return occurredOn;
    }
}
