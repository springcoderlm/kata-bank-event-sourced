package pl.springcoder.bankeventsourced.domain.event;

import pl.springcoder.bankeventsourced.domain.AccountNo;
import pl.springcoder.bankeventsourced.domain.DomainEvent;

import java.util.Date;

public final class AccountDeposited implements DomainEvent {
    private final AccountNo accountNo;
    private final double amount;
    private final Date occurredOn;
    
    public AccountDeposited(AccountNo accountNo, double amount) {
        this.accountNo = accountNo;
        this.amount = amount;
        this.occurredOn = new Date();
    }
    
    public AccountNo accountNo() {
        return accountNo;
    }
    
    public double amount() {
        return amount;
    }
    
    @Override
    public int eventVersion() {
        return 0;
    }
    
    @Override
    public Date occurredOn() {
        return occurredOn;
    }
}
