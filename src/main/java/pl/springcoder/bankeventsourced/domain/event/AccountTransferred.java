package pl.springcoder.bankeventsourced.domain.event;

import pl.springcoder.bankeventsourced.domain.AccountNo;
import pl.springcoder.bankeventsourced.domain.DomainEvent;

import java.util.Date;

public final class AccountTransferred implements DomainEvent {
    private final AccountNo source;
    private final AccountNo dest;
    private final double amount;
    private final Date occurredOn;
    
    public AccountTransferred(AccountNo source, AccountNo dest, double amount) {
        this.source = source;
        this.dest = dest;
        this.amount = amount;
        this.occurredOn = new Date();
    }
    
    public AccountNo source() {
        return source;
    }
    
    public AccountNo dest() {
        return dest;
    }
    
    public double amount() {
        return amount;
    }
    
    @Override
    public int eventVersion() {
        return 0;
    }
    
    @Override
    public Date occurredOn() {
        return occurredOn;
    }
}
