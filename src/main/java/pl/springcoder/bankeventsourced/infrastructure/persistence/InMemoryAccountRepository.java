package pl.springcoder.bankeventsourced.infrastructure.persistence;

import org.springframework.stereotype.Repository;
import pl.springcoder.bankeventsourced.domain.*;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Repository
public class InMemoryAccountRepository implements AccountRepository {
    
    private java.util.Map<AccountNo, Account> accounts = new ConcurrentHashMap<>();
    
    @Override
    public Account withAccountNo(AccountNo anAccountNo) throws AccountDoesNotExistsException {
        Account account = accounts.get(anAccountNo);
        throwExceptionIfItsNull(account);
        return account;
    }
    
    private void throwExceptionIfItsNull(Account account) throws AccountDoesNotExistsException {
        if (Objects.isNull(account)) {
            throw new AccountDoesNotExistsException();
        }
    }
    
    @Override
    public void save(Account anAccount) {
        accounts.put(anAccount.accountNo(), anAccount);
    }
    
    @Override
    public List<AccountData> viewAll() {
        return accounts.entrySet().stream().map(a -> a.getValue().state()).collect(Collectors.toList());
    }
}
