package pl.springcoder.bankeventsourced.interfaces.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.springcoder.bankeventsourced.application.AccountQueryService;
import pl.springcoder.bankeventsourced.application.AccountService;
import pl.springcoder.bankeventsourced.application.command.NewAccountCommand;
import pl.springcoder.bankeventsourced.application.command.TransferCommand;
import pl.springcoder.bankeventsourced.domain.*;
import pl.springcoder.bankeventsourced.interfaces.web.dto.AccountDto;
import pl.springcoder.bankeventsourced.interfaces.web.dto.NewAccountDto;
import pl.springcoder.bankeventsourced.interfaces.web.dto.NewTransferDto;

import javax.validation.Valid;

@RestController
public class AccountResource {
    
    private AccountService accountApplicationService;
    private AccountQueryService accountApplicationQueryService;
    
    public AccountResource(AccountService accountApplicationService, AccountQueryService accountApplicationQueryService) {
        this.accountApplicationService = accountApplicationService;
        this.accountApplicationQueryService = accountApplicationQueryService;
    }
    
    @RequestMapping(value = "/account", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDto create(@Valid @RequestBody NewAccountDto newAccountDto) {
        AccountCurrency accountCurrency = newAccountDto.getAccountCurrency();
        
        AccountData accountData = accountApplicationService.create(new NewAccountCommand(accountCurrency));
        return transform(accountData);
    }
    
    private AccountDto transform(AccountData accountData) {
        return new AccountDto(accountData.getAccountNo(), accountData.getCurrency(), accountData.getBalance());
    }
    
    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public AccountDto details(String accountNo) throws AccountDoesNotExistsException {
        return accountDetails(accountNo);
    }
    
    private AccountDto accountDetails(String sourceAccountNo) throws AccountDoesNotExistsException {
        AccountData accountData = accountApplicationQueryService.details(sourceAccountNo);
        return transform(accountData);
    }
    
    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public AccountDto transfer(@Valid @RequestBody NewTransferDto transferDto)
            throws IncompatibilityAccountCurrencyException, AccountDoesNotExistsException, InsufficientBalanceException {
        String sourceAccountNo = transferDto.getSourceAccountNo();
        String destinationAccountNo = transferDto.getDestinationAccountNo();
        double amount = transferDto.getAmount();
        
        accountApplicationService.transfer(new TransferCommand(sourceAccountNo, destinationAccountNo, amount));
        
        return accountDetails(sourceAccountNo);
    }
}
