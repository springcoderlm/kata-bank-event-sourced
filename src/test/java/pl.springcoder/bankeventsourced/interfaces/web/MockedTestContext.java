package pl.springcoder.bankeventsourced.interfaces.web;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import pl.springcoder.bankeventsourced.application.AccountQueryService;
import pl.springcoder.bankeventsourced.application.AccountService;

public class MockedTestContext {
    
    @Bean
    public AccountService accountService() {
        return Mockito.mock(AccountService.class);
    }
    
    @Bean
    public AccountQueryService accountQueryService() {
        return Mockito.mock(AccountQueryService.class);
    }
}
